package com.project.ui.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.execeptions.PasswordNotFoundException;
import com.project.execeptions.RoleNotFoundException;
import com.project.jpa.UsersRepository;
import com.project.jpa.UsersRoleRepository;
import com.project.ui.model.request.UsersRequestModel;
import com.project.ui.model.response.RolesName;
import com.project.ui.model.response.Users;
import com.project.ui.model.response.UsersRoles;

// This will register this class as REST controller and let us be able
// to receive HTTP request when they match the URL path
@RestController

// This controller will be responsible for the operations that have to do with the user
@RequestMapping("/users")
public class UsersController 
{
	/* First of all, final is a non-access modifier applicable only to a variable, 
	 * a method or a class.Following are different 
	 * contexts where final is used. When a variable 
	 * is declared with final keyword, its value can't be modified, essentially, a constant.
	*/
	private final UsersRepository userRepository; 
	private final UsersRoleRepository roleRepository;
	
	UsersRoles usersStandardRole = new UsersRoles(RolesName.standard);
	UsersRoles usersAdminRole = new UsersRoles(RolesName.admin);
	
	String[] adminRole = {usersAdminRole.getRoleName().toString()};
	String[] standardRole = {usersStandardRole.getRoleName().toString()};
	String[] bothRoles = {adminRole[0], standardRole[0]};
	
	public UsersController(UsersRepository userRepository, UsersRoleRepository roleRepository)
	{
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}
	
	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE,
							MediaType.APPLICATION_XML_VALUE})
	public List<Users> getAllUsers()
	{
		return userRepository.findAll();
	}
	
	@GetMapping(path="/roles", 
				produces = {MediaType.APPLICATION_JSON_VALUE,
							MediaType.APPLICATION_XML_VALUE})
	public List<UsersRoles> getAllRoles()
	{
		return roleRepository.findAll();
	}
	
	@GetMapping(path="/{userID}", 
			produces = {MediaType.APPLICATION_JSON_VALUE,
						MediaType.APPLICATION_XML_VALUE})
	public Optional<Users> getOneUser(@PathVariable Users userID)
	{
		long id = userID.getId();
		return userRepository.findById(id);
	}
	
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE,
				  			 MediaType.APPLICATION_XML_VALUE}, 
				 produces = {MediaType.APPLICATION_JSON_VALUE,
						 	 MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<Users> createUser(@Valid @RequestBody UsersRequestModel userDetails)
	{
		String name = userDetails.getName();
		String password = userDetails.getPassword();

		String[] role = userDetails.getRole();
		
		if (role==null||role.length==0) 
		{
			userDetails.setRole(standardRole);
			role = userDetails.getRole();
			Users tempRepository = new Users(name, password, role);
			
			userRepository.save(tempRepository);
			
			return new ResponseEntity<Users>(tempRepository, HttpStatus.ACCEPTED); 
		}

		if (!role[0].equals("standard"))
		{
			if(!role[0].equals("admin")) 
			{
				throw new RoleNotFoundException(role[0]);	
			}
		}
		
		if (role[0].equals("standard"))
		{
			if(role[1]!=null && !role[1].equals("admin")) 
			{
				throw new RoleNotFoundException(role[1]);	
			}
		}
		
		if (role[0].equals("admin") && password==null ||
		   (role[0].equals("standard") && role[1].equals("admin") && password==null))
		{			
			throw new PasswordNotFoundException();	
		}

		Users tempRepository = new Users(name, password, role);
		
		userRepository.save(tempRepository);
		
		return new ResponseEntity<Users>(tempRepository, HttpStatus.ACCEPTED);
	
//		return new ResponseEntity<Users>(tempRepository, HttpStatus.ACCEPTED);
	}
	
	
}
