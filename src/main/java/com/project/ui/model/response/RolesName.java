package com.project.ui.model.response;

public enum RolesName 
{
	standard("standard"),
	admin("admin");
	
    private final String text;

    /**
     * @param text
     */
    RolesName(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}


/**
	This is how to representate the value of ENUM types into Strings
*/