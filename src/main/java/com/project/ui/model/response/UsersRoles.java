package com.project.ui.model.response;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "TM_ROLES")
public class UsersRoles 
{

	private  @Id int id;
	private RolesName roleName;
	
	public UsersRoles() {}
	
	public UsersRoles (int id, RolesName roleName) 
	{
		this.id = id;
		this.roleName = roleName;
	}
	
	public UsersRoles (RolesName roleName) 
	{
		this.roleName = roleName;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RolesName getRoleName() {
		return roleName;
	}

	public void setRoleName(RolesName roleName) {
		this.roleName = roleName;
	}
}
