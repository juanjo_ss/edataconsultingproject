package com.project.ui.model.response;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Entity
@Data
@Table(name = "TM_USERS")
public class Users 
{
	
	private @Id @GeneratedValue long id;
	private String name;
	private String password;
	private String[] role;
	
	
	public Users() {}
	/**
	 * Users that are admin, have password
	 * and their role will be admin as default
	 * @param name
	 */
	public Users(String name, String password, String[] role)
	{
		this.name = name;
		this.password = password;
		this.role=role;
	}
	
	/**
	 * Users that are not admins, don't have to have password
	 * and their role will be standard as default
	 * @param name
	 */
	public Users(String name, String[] role)
	{
		this.name = name;
		this.role=role;
	}	
	
	public String userInfoLog()
	{
		String response = "ID: " + Long.toString(id) + " Name: " + name + " PWD: " + password +
				          " Role 1: " + role[0];
		
		return response;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String[] getRole() {
		return role;
	}
	public void setRole(String[] role) {
		this.role = role;
	}
	
}


/**
	@Data is a Lombok annotation to create all the getters, setters, equals, hash, and toString methods,
	 based on the fields.
	@Entity is a JPA annotation to make this object ready for storage in a JPA-based data store.
	id, name, and role are the attribute for our domain object, the first being marked with more JPA 
	annotations to indicate it’s the primary key and automatically populated by the JPA provider.
	a custom constructor is created when we need to create a new instance, but don’t yet have an id.
*/