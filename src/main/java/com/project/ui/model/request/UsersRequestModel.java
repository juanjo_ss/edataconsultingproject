package com.project.ui.model.request;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UsersRequestModel 
{
	private @Id @GeneratedValue long id;
	
	@NotNull(message="First Name cannot be empty!!")
	@Size(min=2, message="First Name should be minimun 2 characters")
	private String name;
	
	@Size(min=4, max=8, message="Password should be between 4 and 8 characters")
	private String password;
	
	
	private String[] role;
	
	public UsersRequestModel(){}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String[] getRole() {
		return role;
	}
	public void setRole(String[] role) {
		this.role = role;
	}
	
}

/**
	Esta clase se crea para añadir un objeto temporal que analice los datos que llegan en la Trama POST
	y los incluya en la base de datos si son buenos.
*/

