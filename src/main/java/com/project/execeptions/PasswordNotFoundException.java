package com.project.execeptions;

public class PasswordNotFoundException extends RuntimeException
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 6445382136027701565L;

	public PasswordNotFoundException()
	{
		super("To be an admin you must enter a password");
	}
}
