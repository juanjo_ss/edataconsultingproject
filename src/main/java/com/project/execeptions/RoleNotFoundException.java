package com.project.execeptions;

public class RoleNotFoundException extends RuntimeException  
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3252019143454118036L;

	public RoleNotFoundException(String msg)
	{
		super("This role do not exist: " + msg + ". You must provide a supported role");
	}
}
