package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EDataConsultingProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(EDataConsultingProjectApplication.class, args);
	}

}
