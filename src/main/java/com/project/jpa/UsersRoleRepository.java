package com.project.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.ui.model.response.UsersRoles;

public interface UsersRoleRepository extends JpaRepository<UsersRoles, Long>
{

}
