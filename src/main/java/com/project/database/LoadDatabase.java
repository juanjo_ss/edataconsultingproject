package com.project.database;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import com.project.jpa.UsersRepository;
import com.project.jpa.UsersRoleRepository;
import com.project.ui.model.response.RolesName;
import com.project.ui.model.response.Users;
import com.project.ui.model.response.UsersRoles;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase 
{
	UsersRoles usersStandardRole = new UsersRoles(RolesName.standard);
	UsersRoles usersAdminRole = new UsersRoles(RolesName.admin);
	
	String[] adminRole = {usersAdminRole.getRoleName().toString()};
	String[] standardRole = {usersStandardRole.getRoleName().toString()};
	String[] bothRoles = {adminRole[0], standardRole[0]};
	
 	@Bean
	CommandLineRunner initDatabase(UsersRepository usersRepository, UsersRoleRepository usersRoleRepository)
	{
		return args ->
		{
			System.out.println("Preloading " + usersRoleRepository.save(new UsersRoles(1, RolesName.standard)));
			System.out.println("Preloading " + usersRoleRepository.save(new UsersRoles(2, RolesName.admin)));
			
			System.out.println();
			
			System.out.println("Preloading " + usersRepository.save(new Users("admin", "admin", adminRole)).userInfoLog());
			System.out.println("Preloading " + usersRepository.save(new Users("Juanjo", "1234", bothRoles)).userInfoLog());
			System.out.println("Preloading " + usersRepository.save(new Users("Pedro", standardRole)).userInfoLog());
			System.out.println("Preloading " + usersRepository.save(new Users("Marcos", standardRole)).userInfoLog());
		};
	}
	
}
/**
	What happens when it gets loaded?
	Spring Boot will run ALL CommandLineRunner beans once the application context is loaded.
	This runner will request a copy of the EmployeeRepository you just created.
	Using it, it will create two entities and store them.
	@Slf4j is a Lombok annotation to autocreate an Slf4j-based LoggerFactory as log, allowing us to log these newly created "employees".
 */
